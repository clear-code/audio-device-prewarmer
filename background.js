let matcher = /^https:\/\/meet\.google\.com\//;
chrome.storage.managed.get(['domain'], function(result) {
  if (result.domain)
    matcher = new RegEpx('^https://' + result.domain + '/');
});

chrome.webNavigation.onCommitted.addListener(function(details) {
   if (matcher.test(details.url)) {
        chrome.tabs.executeScript(details.tabId, {
            code: "navigator.mediaDevices.getUserMedia({audio:true})"
        });
    }
});
