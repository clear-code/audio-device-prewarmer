.PHONY: zip

all: zip

zip:
	rm -f ./*.zip
	zip -r -9 audio-device-prewarmer.zip manifest.json background.js -x '*/.*' >/dev/null 2>/dev/null
